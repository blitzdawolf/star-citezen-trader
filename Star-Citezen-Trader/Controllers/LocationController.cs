﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StarCitezenTrader.Context;
using StarCitezenTrader.Dal;
using StarCitezenTrader.Models;

namespace StarCitezenTrader.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        private readonly LocationDal location;
        public LocationController(TraderContext traderContext)
        {
            location = new LocationDal(traderContext);
        }

        [Route("create")]
        public ActionResult create()
        {
            /*traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738202", Description = "", Name = "ArcCorp Mining Area 141", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881a" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381fa", Description = "", Name = "ArcCorp Mining Area 157", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881b" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738209", Description = "", Name = "Ashburn Channel Emergency Shelter", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8819" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381fc", Description = "", Name = "Aston Ridge Emergency Shelter", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881b" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381fd", Description = "", Name = "Benson Mining Outpost", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881b" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738203", Description = "", Name = "Bountiful Harvest Hydroponics", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881a" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381f0", Description = "", Name = "Comm Array 126", PadType = Models.Location.padType.None, Type = Models.Location.type.CommArray, ContainerId = "5bb6858bbeca6870d34e8816" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381f1", Description = "", Name = "Comm Array 275", PadType = Models.Location.padType.None, Type = Models.Location.type.CommArray, ContainerId = "5bb6858bbeca6870d34e8816" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381f2", Description = "", Name = "Comm Array 306", PadType = Models.Location.padType.None, Type = Models.Location.type.CommArray, ContainerId = "5bb6858bbeca6870d34e8816" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381f3", Description = "", Name = "Comm Array 472", PadType = Models.Location.padType.None, Type = Models.Location.type.CommArray, ContainerId = "5bb6858bbeca6870d34e8816" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738222", Description = "", Name = "Comm Array ST1-13", PadType = Models.Location.padType.None, Type = Models.Location.type.CommArray, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738223", Description = "", Name = "Comm Array ST1-48", PadType = Models.Location.padType.None, Type = Models.Location.type.CommArray, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738224", Description = "", Name = "Comm Array ST1-61", PadType = Models.Location.padType.None, Type = Models.Location.type.CommArray, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738225", Description = "", Name = "Comm Array ST1-92", PadType = Models.Location.padType.None, Type = Models.Location.type.CommArray, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381f8", Description = "", Name = "Covalex Shipping Hub Gundo", PadType = Models.Location.padType.Medium, Type = Models.Location.type.SpaceStation, ContainerId = "5bb6858bbeca6870d34e881a" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381fe", Description = "", Name = "Deakins Research Outpost", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881b" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738204", Description = "", Name = "Dunlow Ridge Emergency Shelter", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881a" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738205", Description = "", Name = "Eager Flats Emergency Shelter", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881a" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873820a", Description = "", Name = "Flanagans Ravine Emergency Shelter", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8819" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873820b", Description = "", Name = "Gallete Family Farms", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8819" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381f7", Description = "", Name = "GrimHEX", PadType = Models.Location.padType.Medium, Type = Models.Location.type.SpaceStation, ContainerId = "5bb6858bbeca6870d34e881b" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873822e", Description = "", Name = "HDMS Anderson", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881d" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873822d", Description = "", Name = "HDMS Bezdek", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881c" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738226", Description = "", Name = "HDMS Edmond", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738227", Description = "", Name = "HDMS Hadley", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738230", Description = "", Name = "HDMS Hahn", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881e" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873822c", Description = "", Name = "HDMS Lathan", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881c" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873822f", Description = "", Name = "HDMS Norgaard", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881d" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738228", Description = "", Name = "HDMS Oparei", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738231", Description = "", Name = "HDMS Perlman", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881e" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738229", Description = "", Name = "HDMS Pinewood", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738233", Description = "", Name = "HDMS Ryder", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881f" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873822a", Description = "", Name = "HDMS Stanhope", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873822b", Description = "", Name = "HDMS Thedus", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738232", Description = "", Name = "HDMS Woodruff", PadType = Models.Location.padType.None, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881f" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873820c", Description = "", Name = "Hickes Research Outpost", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8819" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873820d", Description = "", Name = "Julep Ravine Emergency Shelter", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8819" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381fb", Description = "", Name = "Jumptown", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881b" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381ff", Description = "", Name = "Kosso Basin Emergency Shelter", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881b" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738221", Description = "", Name = "Kudre Ore", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881a" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381f4", Description = "", Name = "Levski", PadType = Models.Location.padType.Large, Type = Models.Location.type.LandingZone, ContainerId = "5bb6858bbeca6870d34e8814" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381f5", Description = "", Name = "Lorville", PadType = Models.Location.padType.Large, Type = Models.Location.type.LandingZone, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873820e", Description = "", Name = "Mogote Emergency Shelter", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8819" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738200", Description = "", Name = "Nakamura Valley Emergency Shelter", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881b" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381f6", Description = "", Name = "Port Olisar", PadType = Models.Location.padType.Large, Type = Models.Location.type.SpaceStation, ContainerId = "5bb6858bbeca6870d34e8816" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738220", Description = "", Name = "R&R CRU-L1", PadType = Models.Location.padType.Large, Type = Models.Location.type.TruckStop, ContainerId = "5bb6858bbeca6870d34e8816" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738239", Description = "", Name = "R&R CRU-L3", PadType = Models.Location.padType.Large, Type = Models.Location.type.TruckStop, ContainerId = "5bb6858bbeca6870d34e8816" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873823a", Description = "", Name = "R&R CRU-L4", PadType = Models.Location.padType.Large, Type = Models.Location.type.TruckStop, ContainerId = "5bb6858bbeca6870d34e8816" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738234", Description = "", Name = "R&R CRU-L5", PadType = Models.Location.padType.Large, Type = Models.Location.type.TruckStop, ContainerId = "5bb6858bbeca6870d34e8816" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873823c", Description = "", Name = "R&R HUR-L1", PadType = Models.Location.padType.Large, Type = Models.Location.type.TruckStop, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738235", Description = "", Name = "R&R HUR-L2", PadType = Models.Location.padType.Large, Type = Models.Location.type.TruckStop, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738236", Description = "", Name = "R&R HUR-L3", PadType = Models.Location.padType.Large, Type = Models.Location.type.TruckStop, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738237", Description = "", Name = "R&R HUR-L4", PadType = Models.Location.padType.Large, Type = Models.Location.type.TruckStop, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873823b", Description = "", Name = "R&R HUR-L5", PadType = Models.Location.padType.Large, Type = Models.Location.type.TruckStop, ContainerId = "5bb6858bbeca6870d34e8817" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e87381f9", Description = "", Name = "Security Post Kareah", PadType = Models.Location.padType.Medium, Type = Models.Location.type.SpaceStation, ContainerId = "5bb6858bbeca6870d34e8819" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738206", Description = "", Name = "Shubin Mining Facility SCD-1", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881a" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738201", Description = "", Name = "Talarine Divide Emergency Shelter", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881b" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738207", Description = "", Name = "Tamdon Plains Emergency Shelter", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881a" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873820f", Description = "", Name = "Terra Mills HydroFarm", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8819" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e873821f", Description = "", Name = "Tram & Myers Mining", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e8819" });
            traderContext.Locations.Add(new Models.Location() { Id = "5bb75803b6551685e8738208", Description = "", Name = "Wolf Point Emergency Shelter", PadType = Models.Location.padType.Large, Type = Models.Location.type.Outpost, ContainerId = "5bb6858bbeca6870d34e881a" });

            traderContext.SaveChanges();*/

            return Ok();
        }

        public ActionResult<List<Location>> index()
        {
            return location.GetlcoationWithCommodities();
            return location.GetLocations();
        }
    }
}