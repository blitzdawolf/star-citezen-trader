﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StarCitezenTrader.Context;
using StarCitezenTrader.Dal;
using StarCitezenTrader.Models;
using static StarCitezenTrader.Dal.PlannerDal;

namespace StarCitezenTrader.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlannerController : ControllerBase
    {
        private readonly PlannerDal planner;
        public PlannerController(TraderContext traderContext)
        {
            planner = new PlannerDal(traderContext);
            // this.traderContext = traderContext;
        }

        public ActionResult index()
        {
            return Ok();
        }

        [Route("plan/{starter}/{money}/{scu}")]
        [HttpPost]
        public ActionResult<ProfitPlanner> plan(string starter, int money, int scu, string[] ignore, Location.padType padType = Location.padType.None)
        {
            ProfitPlanner planner = this.planner.PlanRoute(starter, money, scu * 100, ignore);
            return planner;
        }
    }
}