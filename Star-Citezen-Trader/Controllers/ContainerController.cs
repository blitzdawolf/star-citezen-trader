﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StarCitezenTrader.Context;

namespace StarCitezenTrader.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContainerController : ControllerBase
    {
        private readonly TraderContext traderContext;
        public ContainerController(TraderContext traderContext)
        {
            this.traderContext = traderContext;
        }

        [Route("create")]
        public ActionResult create()
        {
            traderContext.Containers.Add(new Models.Container() { Id = "5bb6858bbeca6870d34e8813", Name = "Stanton", Description = "The only home we got!", Type = Models.Container.type.System });

            traderContext.Containers.Add(new Models.Container() { Id = "5bb6858bbeca6870d34e8815", Name = "ArcCorp", Description = "", Type = Models.Container.type.Planet, ParentId = "5bb6858bbeca6870d34e8813" });
            traderContext.Containers.Add(new Models.Container() { Id = "5bb6858bbeca6870d34e8816", Name = "Crusader", Description = "", Type = Models.Container.type.Planet, ParentId = "5bb6858bbeca6870d34e8813" });
            traderContext.Containers.Add(new Models.Container() { Id = "5bb6858bbeca6870d34e8817", Name = "Hurston", Description = "", Type = Models.Container.type.Planet, ParentId = "5bb6858bbeca6870d34e8813" });
            traderContext.Containers.Add(new Models.Container() { Id = "5bb6858bbeca6870d34e8818", Name = "Microtech", Description = "", Type = Models.Container.type.Planet, ParentId = "5bb6858bbeca6870d34e8813" });

            traderContext.Containers.Add(new Models.Container() { Id = "5bb6858bbeca6870d34e881d", Name = "Aberdeen", Description = "", Type = Models.Container.type.Moon, ParentId = "5bb6858bbeca6870d34e8817" });
            traderContext.Containers.Add(new Models.Container() { Id = "5bb6858bbeca6870d34e881c", Name = "Arial", Description = "", Type = Models.Container.type.Moon, ParentId = "5bb6858bbeca6870d34e8817" });
            traderContext.Containers.Add(new Models.Container() { Id = "5bb6858bbeca6870d34e8819", Name = "Celin", Description = "", Type = Models.Container.type.Moon, ParentId = "5bb6858bbeca6870d34e8816" });
            traderContext.Containers.Add(new Models.Container() { Id = "5bb6858bbeca6870d34e881a", Name = "Daymar", Description = "", Type = Models.Container.type.Moon, ParentId = "5bb6858bbeca6870d34e8816" });
            traderContext.Containers.Add(new Models.Container() { Id = "5bb6858bbeca6870d34e881f", Name = "Ita", Description = "", Type = Models.Container.type.Moon, ParentId = "5bb6858bbeca6870d34e8817" });
            traderContext.Containers.Add(new Models.Container() { Id = "5bb6858bbeca6870d34e881e", Name = "Magda", Description = "", Type = Models.Container.type.Moon, ParentId = "5bb6858bbeca6870d34e8817" });
            traderContext.Containers.Add(new Models.Container() { Id = "5bb6858bbeca6870d34e881b", Name = "Yela", Description = "", Type = Models.Container.type.Moon, ParentId = "5bb6858bbeca6870d34e8816" });

            traderContext.Containers.Add(new Models.Container() { Id = "5bb6858bbeca6870d34e8814", Name = "Delamar", Description = "", Type = Models.Container.type.Asteroid, ParentId = "5bb6858bbeca6870d34e8813" });
            traderContext.SaveChanges();
            return Ok();
        }

        public ActionResult index()
        {
            List<Models.Container> test = traderContext.Containers
                // .Include(x=>x.Locations)
                .ToList();
            return new JsonResult(test);
        }

        [Route("{id}")]
        public ActionResult<Models.Container> getContainer(string id)
        {
            Models.Container container = traderContext.Containers
                .Include(x => x.Locations)
                .FirstOrDefault(x => x.Id == id);
            return container;
        }
    }
}