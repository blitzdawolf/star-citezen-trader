﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace StarCitezenTrader.Models
{
    public class Location
    {
        public enum padType
        {
            Smal,
            Medium,
            Large,
            None
        }
        public enum type
        {
            Outpost,
            TruckStop,
            SpaceStation,
            LandingZone,
            CommArray
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public string ContainerId { get; set; }
        [ForeignKey("ContainerId")]
        public Container Container { get; set; }

        public padType PadType { get; set; }
        public type Type { get; set; }

        [ForeignKey("LocationId")]
        public List<CommodityPrices> CommodityPrices { get; set; }
    }
}
