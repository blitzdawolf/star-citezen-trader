﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace StarCitezenTrader.Models
{
    public class Container
    {
        public enum type
        {
            Moon,
            Planet,
            System,
            Asteroid
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ParentId { get; set; }
        public Container Parent { get; set; }

        [ForeignKey("ContainerId")]
        public List<Location> Locations { get; set; }
        public type Type { get; set; }
    }
}
