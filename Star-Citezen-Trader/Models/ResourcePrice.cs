﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarCitezenTrader.Models
{
    public class ResourcePrice
    {
        public string Id { get; set; }
        public Location Location { get; set; }
        public Resource Resource { get; set; }
        public bool Sell { get; set; }
        public float Price { get; set; }
    }
}
