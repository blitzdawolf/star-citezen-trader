﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarCitezenTrader.Models
{
    public class Commoditie
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Illegal { get; set; }
    }
}
