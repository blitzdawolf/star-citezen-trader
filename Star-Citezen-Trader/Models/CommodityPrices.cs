﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace StarCitezenTrader.Models
{
    public class CommodityPrices
    {
        public enum type
        {
            Sell,
            Buy
        }
        public string Id { get; set; }
        public string CommodityId { get; set; }
        public Commoditie Commodity { get; set; }
        public DateTime Date { get; set; }
        public string LocationId { get; set; }
        [ForeignKey("LocationId")]
        public Location Location { get; set; }
        public type Type { get; set; }
        public float Price { get; set; }
    }
}
