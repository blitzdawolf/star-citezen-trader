﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarCitezenTrader.Models
{
    public class Resource
    {
        public enum type
        {
            Mineral,
            Gas,
            Metal,
            Liquid
        }
        public string Id { get; set; }
        public string name { get; set; }
        public type Type { get; set; }
    }
}
