﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StarCitezenTrader.Models;

namespace StarCitezenTrader.Context
{
    public class TraderContext : DbContext
    {
        public virtual DbSet<Commoditie> Commodities { get; set; }
        public virtual DbSet<CommodityPrices> CommodityPrices { get; set; }
        public virtual DbSet<Container> Containers { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<MissionGiver> MissionGivers { get; set; }
        public virtual DbSet<Resource> Resources { get; set; }
        public virtual DbSet<ResourcePrice> ResourcePrices { get; set; }

        public List<CommodityPrices> GetAllCommodityPrices(bool allowIlagel = false)
        {
            List<CommodityPrices> x = CommodityPrices.ToList();
            foreach (var item in x)
            {
                item.Commodity = Commodities.FirstOrDefault(y => y.Id == item.CommodityId);
                item.Location = Locations.FirstOrDefault(y => y.Id == item.LocationId);
            }
            if (!allowIlagel)
            {
                x = x.Where(t => t.Commodity.Illegal == false).ToList();
            }
            return x;
        }

        public List<CommodityPrices> GetAllBuyPrices(bool allowIlagel = false)
        {
            return GetAllCommodityPrices(allowIlagel).Where(x => x.Type == Models.CommodityPrices.type.Buy).ToList();
        }

        public List<CommodityPrices> GetAllSellyPrices(bool allowIlagel = false)
        {
            return GetAllCommodityPrices(allowIlagel).Where(x => x.Type == Models.CommodityPrices.type.Sell).ToList();
        }

        public TraderContext(DbContextOptions<TraderContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("User ID=user;Password=123456789;Server==192.168.0.151;Port=5432;Database=trader;Integrated Security=true; Pooling=true;");
            }
            base.OnConfiguring(optionsBuilder);
        }
    }
}
