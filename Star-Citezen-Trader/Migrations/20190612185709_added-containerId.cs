﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StarCitezenTrader.Migrations
{
    public partial class addedcontainerId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContainerId",
                table: "Locations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Locations_ContainerId",
                table: "Locations",
                column: "ContainerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_Containers_ContainerId",
                table: "Locations",
                column: "ContainerId",
                principalTable: "Containers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Locations_Containers_ContainerId",
                table: "Locations");

            migrationBuilder.DropIndex(
                name: "IX_Locations_ContainerId",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "ContainerId",
                table: "Locations");
        }
    }
}
