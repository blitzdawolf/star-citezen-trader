﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StarCitezenTrader.Migrations
{
    public partial class fixedPrices : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sell",
                table: "CommodityPrices");

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "CommodityPrices",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "CommodityPrices");

            migrationBuilder.AddColumn<bool>(
                name: "Sell",
                table: "CommodityPrices",
                nullable: false,
                defaultValue: false);
        }
    }
}
