﻿using Microsoft.EntityFrameworkCore;
using StarCitezenTrader.Context;
using StarCitezenTrader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarCitezenTrader.Dal
{
    public class CommoditiePriceDal
    {
        private readonly TraderContext traderContext;
        public CommoditiePriceDal(TraderContext traderContext)
        {
            this.traderContext = traderContext;
        }

        public List<CommodityPrices> GetAll()
        {
            return traderContext
                    .CommodityPrices
                    .Include(x => x.Location)
                    .Include(x => x.Commodity)
                    .ToList();
        }

        public List<CommodityPrices> GetFromLocation(string location)
        {
            return GetAll()
                    .Where(x => x.LocationId == location)
                    .ToList();
        }

        public List<CommodityPrices> SellPrices()
        {
            var t = GetAll()
                    .Where(x => x.Type == CommodityPrices.type.Sell)
                    .ToList();
            return GetAll()
                    .Where(x => x.Type == CommodityPrices.type.Sell)
                    .ToList();
        }

        public List<CommodityPrices> BuyPriceces()
        {
            return GetAll()
                    .Where(x => x.Type == CommodityPrices.type.Buy)
                    .ToList();
        }
    }
}
