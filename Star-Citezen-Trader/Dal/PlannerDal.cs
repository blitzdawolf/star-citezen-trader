﻿using StarCitezenTrader.Context;
using StarCitezenTrader.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace StarCitezenTrader.Dal
{
    public class PlannerDal
    {
        private readonly TraderContext traderContext;
        private readonly LocationDal location;
        private readonly CommoditiePriceDal commoditiePrice;

        public struct Profit
        {
            public CommodityPrices BuyCommodityPrice { get; set; }
            public CommodityPrices SelCommodityPrice { get; set; }

            public int BuyAmout { get; set; }

            public float BuyPrice => BuyCommodityPrice.Price * BuyAmout;
            public float SelPrice => SelCommodityPrice.Price * BuyAmout;
            public float ProfitAmt => SelPrice - BuyPrice;
        }

        public struct ProfitPlanner
        {
            public int Jumps { get; set; }
            public float Profit => CalculateProfit();
            public int ProfitFloored => (int)Math.Floor(Profit);

            public List<Profit> Profits { get; set; }

            float CalculateProfit()
            {
                if (Profits == null)
                    return 0;

                float totalProfit = 0;
                Profits.ForEach(item => totalProfit += item.ProfitAmt);
                return totalProfit;
            }
        }

        public PlannerDal(TraderContext traderContext)
        {
            this.traderContext = traderContext;
            commoditiePrice = new CommoditiePriceDal(traderContext);
            location = new LocationDal(traderContext);
        }

        public ProfitPlanner PlanRoute(string location, float money, int Scu, string[] ignore)
        {
            Location startLocation = this.location.GetLcoation(location);
            List<CommodityPrices> startCommoditys = startLocation
                                                        .CommodityPrices
                                                        .Where(x=>x.Type== CommodityPrices.type.Buy)
                                                        .ToList();

            List<ProfitPlanner> planners = new List<ProfitPlanner>();

            startCommoditys.ForEach(x =>
            {
                ProfitPlanner planner = new ProfitPlanner() { Profits = new List<Profit>() };
                CommodityPrices best = BestSellPrice(x, ignore);
                Profit p = new Profit()
                {
                    BuyCommodityPrice = x,
                    SelCommodityPrice = best,
                    BuyAmout = 1
                };

                while (p.BuyPrice < money )
                {
                    if (p.BuyAmout >= Scu)
                        break;
                    p.BuyAmout++;
                }
                p.BuyAmout--;

                planner.Profits.Add(p);

                planners.Add(planner);
            });

            // Add some logic here

            return BestProfit(planners);
        }

        public ProfitPlanner BestProfit(List<ProfitPlanner> planners)
        {
            ProfitPlanner best = planners[0];

            planners.ForEach(x =>
            {
                if (x.Profit > best.Profit)
                {
                    best = x;
                }
            });

            return best;
        }

        public CommodityPrices BestSellPrice(CommodityPrices prices, string[] ignore)
        {
            List<CommodityPrices> allSellPlaces = commoditiePrice.SellPrices()
                                                    .Where(x=>x.CommodityId == prices.CommodityId)
                                                    .ToList();

            CommodityPrices currentBest = null;

            allSellPlaces.ForEach(x =>
            {
                if ((currentBest == null || currentBest.Price < x.Price) && !ignore.Contains(x.LocationId))
                {
                    currentBest = x;
                }
            });

            return currentBest;
        }
    }
}
