﻿using Microsoft.EntityFrameworkCore;
using StarCitezenTrader.Context;
using StarCitezenTrader.Models;
using System.Collections.Generic;
using System.Linq;

namespace StarCitezenTrader.Dal
{
    public class LocationDal
    {
        private readonly TraderContext traderContext;
        public LocationDal(TraderContext traderContext)
        {
            this.traderContext = traderContext;
        }

        public List<Location> GetLocations()
        {
            var test = traderContext.
                Locations.
                    Where(x => x.PadType != Location.padType.None).
                    Include(location => location.Container).
                    Include(location => location.CommodityPrices).
                ToList();
            foreach (var item in test)
            {
                item.CommodityPrices.ForEach(x => {
                    x.Location = null;
                    x.Commodity = traderContext.Commodities.Where(y => y.Id == x.CommodityId).FirstOrDefault();
                });
                item.Container.Locations = null;
            }
            return test;
        }

        public List<Location> GetlcoationWithCommodities()
        {
            return GetLocations().
                    Where(x => x.CommodityPrices.Count > 0).
                    ToList();
        }

        public Location GetLcoation(string id)
        {
            return GetlcoationWithCommodities().FirstOrDefault(x => x.Id == id);
        }
    }
}
